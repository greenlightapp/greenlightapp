var rawData = [];
var hardBreakData = new Array();
var locationID;

$(function() {
	$(document).ready(function() {
		Highcharts.setOptions({
			global : {
				useUTC : false
			}
		});

		$('#braking').highcharts({
			chart : {
				type : 'spline',
				animation : Highcharts.svg, // don't animate in old IE
				marginRight : 10,
				events : {
					load : function() {

						// set up the updating of the chart each second
						var series = this.series[0];
						setInterval(function() {
							var x = (new Date()).getTime(), // current time
							y = hardBreakData.pop().getRating();
							console.log(x, y);
							console.log(hardBreakData);
							series.addPoint([x, y], true, true);
						}, 4000);
					}
				}
			},
			title : {
				text : 'Live Data'
			},
			xAxis : {
				type : 'datetime',
				tickPixelInterval : 150
			},
			yAxis : {
				title : {
					text : 'Value'
				},
				plotLines : [{
					value : 0,
					width : 1,
					color : '#808080'
				}]
			},
			tooltip : {
				formatter : function() {
					return '<b>' + this.series.name + '</b><br/>' + Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' + Highcharts.numberFormat(this.y, 2);
				}
			},
			legend : {
				enabled : false
			},
			exporting : {
				enabled : false
			},
			series : [{
				name : 'Braking data',
				data : ( function() {
						// generate an array of random data
						var data = [], time = (new Date()).getTime(), i;

						for ( i = -19; i <= 0; i += 1) {
							data.push({
								x : time + i * 1000,
								y : Math.random()
							});
						}
						return data;
					}())
			}]
		});
	});
});

$(function() {
	$('#constantspeed').highcharts({
		chart : {
			type : 'line'
		},
		title : {
			text : 'Constant Speed Metrix'
		},
		xAxis : {
			title : {
				text : 'Time'
			}
		},
		yAxis : {
			title : {
				text : 'Speed Events'
			}
		},
		series : [{
			name : 'Event',
			data : [4, 5, 3, 2, 4, 4]
		}]
	});
});

var dataPoint = function(newlatitude, newlongitude, newtimestamp, newspeed, newacceleration){
   // console.log(newlatitude,newlongitude, newtimestamp);
    var latitude = newlatitude;
    var longitude = newlongitude;
    var timestamp = newtimestamp;
    var speed = newspeed;
    var acceleration = newacceleration;


    getLatitude = function(){
        return latitude;
    }
    getLongitude = function(){
        return longitude;
    }
    getTimestamp = function(){
        return timestamp;
    }
    getSpeed = function(){
    //    console.log(speed);
        return speed;
    }
    getAcceleration = function(){
        return acceleration;
    }
    setSpeed = function(newSpeed){
        speed = newSpeed; 
    }

    setAcceleration = function(newAcceleration){
        acceleration = newAcceleration;
    }

  //  console.log(getLongitude());

    return {
        getLatitude: getLatitude,
        getLongitude: getLongitude,
        getTimestamp: getTimestamp,
        setSpeed: setSpeed,
        getSpeed: getSpeed,
        setAcceleration: setAcceleration,
        getAcceleration: getAcceleration,
        latitude: latitude,
        longitude: longitude,
        timestamp: timestamp
    };
}

function hardBreakDataPoint(timestamp, rating){
    this.timestamp = timestamp;
    this.rating = rating;

    this.getTimestamp = function(){
        return timestamp;
    }
    this.getRating = function(){
        return rating;
    }
}

function initGeo() {
    locationID = navigator.geolocation.watchPosition(
        geosuccess,
        geofailure,
        {
            enableHighAccuracy:true,
            maximumAge:30000,
            timeout:20000 // do we need to alter these timeouts and maximumAges? e.g. if the location API is more busy, will it not by default be giving us new data every 3 seconds?
        }
    );
 
    //moveSpeed(30);
    //moveCompassNeedle(56);
}
 
    var i = 0;
function geosuccess(position) {

    //store most recent data point into the DOM
        var d = new dataPoint(position.coords.latitude, position.coords.longitude, position.timestamp, null, null);
        //console.log(JSON.stringify(d));
        //console.log(position.coords.latitude + " " + position.coords.longitude + " " + position.timestamp);
        rawData.push(new dataPoint(position.coords.latitude, position.coords.longitude, position.timestamp, null, null));
        
      //  console.log("r: ", rawData);
        $("#debugOutput").html($("#debugOutput").html() + i +":" + position.coords.latitude + ": " + position.coords.longitude);

    //calculate the speed at the previous data point via linear approximation.
        var previousLatitude, previousLongitude, nextLatitude, nextLongitude, distance, timeDifference, speed;
        if (i >=2){ //only if there is a data point two data points ago to do the calculation for the previous data point's speed (this is rawData[i -1])
            previousLatitude = rawData[i - 2].getLatitude();
            previousLongitude = rawData[i - 2].getLongitude();
            nextLatitude = rawData[i].getLatitude();
            nextLongitude = rawData[i].getLongitude();
            distance = calculateDistance(previousLatitude, previousLongitude, nextLatitude, nextLongitude);
            timeDifference = calculateTimeDifference(rawData[i].getTimestamp(), rawData[i-2].getTimestamp());
            speed = distance / timeDifference; //meters per second
            rawData[i-1].setSpeed(speed);
         //   console.log(i + ": distance: " + distance + ", speed: " + speed);
          //  console.log("rawdata version" + i + ": distance: " + distance + ", speed: " + + rawData[i-1].getSpeed());
            $("#debugOutput").html($("#debugOutput").html() + " " + ": distance: " + distance + ", speed: " + speed);
        }
    //calculate acceleration for two points ago
    var previousSpeed, nextSpeed, speedDifference, timeDifference, acceleration;
        if (i >=3){//only if there is a data point three data points ago to do the calculation for the acceleration at two point's ago. the point which is rawData[i-2]
        //    console.log(rawData[i-3].getSpeed());
            previousSpeed = rawData[i - 3].getSpeed();
            nextSpeed = rawData[i - 1].getSpeed();

        //    console.log("previous speed from rawdata:", previousSpeed, "nextSpeed from rawdata", nextSpeed);
         //   console.log("previousSpeed: ", previousSpeed, "nextSpeed: ", nextSpeed);

            speedDifference = nextSpeed - previousSpeed;
            timeDifference = calculateTimeDifference(rawData[i-3].getTimestamp(), rawData[i-1].getTimestamp());
            acceleration = speedDifference / timeDifference;
            rawData[i-2].setAcceleration(acceleration);
         //   console.log(i + ": speedDifference: " + speedDifference + ", acceleration: " + acceleration);
            $("#debugOutput").html($("#debugOutput").html() + " --- " + " speedDifference: " + speedDifference + ", acceleration: " + rawData[i-2].getAcceleration());
        }
    //calculate if the number of hard breaks in the past 5 minutes with a threshold of .1 m/s/s acceleration
    //store the result in an object and print out the current amount of hard breaks
   // console.
    

  // console.log("number of hard breaks", getNumberOfHardBreaks(rawData, .1, 5) );
    // console.log("raw data near the end of the loop is", rawData);
  //  console.log("raw data is", rawData);
   // console.log("hardBreakdata is ", hardBreakData);
    var numberOfHardBreaks = getNumberOfHardBreaks(rawData, .1, 5);
   // console.log("number of hard breaks for this time is", numberOfHardBreaks);
    hardBreakData.push(new hardBreakDataPoint(position.timestamp, getNumberOfHardBreaks(rawData, .01, 5)));
   // $("#hardBreaksMetric").text(hardBreakData[i]);
    
     $("#debugOutput").html($("#debugOutput").html() + "<br>");
    i++;
}

function geofailure(position){
   // console.log("Something went wrong with the geolocation: " + new Date().getTime());
}


//@returns number of times there is an acceleration/deceleration over the @threshold acceleration (meters/s/s). Only counts hte nubmer of these occurences in the past @minutes.
function getNumberOfHardBreaks(data, threshold, minutes){
        //console.log("the data passed into the getnumber of hardbreaks function was ", JSON.stringify(data));
        var count = 0;
        var currentTime = new Date().getTime();
        var datapoint;
        for (datapoint in data){
            //    console.log("getAcceleration: ", data[datapoint].getAcceleration(), " is within time range", ((currentTime - data[datapoint].getTimestamp()) / 1000 / 60 < minutes));
            //    console.log("count is ",count);
           // console.log("datapoint speed: " + JSON.stringify(data[datapoint].getSpeed()));

           //if datapoints acceleration is over threshold . .. and if the current time is over
            if (data[datapoint].getAcceleration() > threshold){
                var tempAcc = data[datapoint].getAcceleration();
                count++;
               // alert("acc was fast enought");
              //$("#debugOutput").html($("#debugOutput").html() + "aceleration was" + tempAcc);
            }
           // else            alert("acceleration was not fast enought");
        }
        return count;
}

//may want to ignore curvature of earth in future, for less CPU intensive calculations
function calculateDistance (lat1, lon1, lat2, lon2){  // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d * 1000; // meters
}

function calculateTimeDifference (timeStamp1, timeStamp2){//two linux timestamps in milliseconds since epoch
    return (timeStamp2 - timeStamp1) / 1000; //seconds
}

$(function() {
	//actual code that runs and then calls everything else;
	initGeo();
});

