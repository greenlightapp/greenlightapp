var GasData = function(){
    /**
 * Gas Data from  http://www.mygasfeed.com/

 http://www.mygasfeed.com/keys/api, using the dev API key "rfej9napna"
 Greenlight Production API key: wq1bip797s

  dev:  http://devapi.mygasfeed.com/stations/radius/39.9636223/-82.9898594/5/reg/Price/rfej9napna.json?callback=?
  prod: http://devapi.mygasfeed.com/stations/radius/39.9636223/-82.9898594/5/reg/Price/wq1bip797s.json?callback=?

  parameters:
  lat: latitude of the center of the search zone
  lon: longitude of the center of the search zone
  radius: search zone radius in miles
  callback: name/reference to a function that will be called when the async data call returns

  NOTE - there is no real error handeling here.  We shoud be checking the "status" element of the return object, as well as adding an .error block on the ajax call

 */

    var GasData;
    var currentGasPrices = {};
    var getAverageGasPrice = function(lat, lon, radius, callback) {

	// this is the development API
	//var theUrl = "http://devapi.mygasfeed.com/stations/radius/" + lat + "/" + lon + "/" + radius + "/reg/Price/rfej9napna.json?callback=?";

	//this is the
	var theUrl = "http://api.mygasfeed.com/stations/radius/" + lat + "/" + lon + "/" + radius + "/reg/Price/wq1bip797s.json?callback=?";

	$.ajax({
	    dataType: "json",
	    url: theUrl,
	    success: function(data) {
		//console.log(data);
		currentGasPrices = _procGasData(data);
		callback(currentGasPrices);
	  }
	});

    };

    _procGasData = function (theData) {
	var stationList = theData['stations'];
	var collection = {
	    reg: {
		min: 10,
		max: 0,
		avg: 0
	    },
	    mid: {
		min: 10,
		max: 0,
		avg: 0
	    },
	    pre: {
		min: 10,
		max: 0,
		avg: 0
	    },
	    diesel: {
		min: 10,
		max: 0,
		avg: 0
	    },
	    stations: []
	};

	var countMid = 0;
	var countReg = 0;
	var countPre = 0;
	var countDie = 0;
	var theMidPrice = 0;
	var thePrePrice = 0;
	var theRegPrice = 0;
	var theDiePrice = 0;

	for (i in stationList) {
	    var theStation = stationList[i];
	    //console.log(theStation);
	    collection.stations.push(theStation);
	    theMidPrice = parseFloat(theStation.mid_price);
	    thePrePrice = parseFloat(theStation.pre_price);
	    theRegPrice = parseFloat(theStation.reg_price);
	    theDiePrice = parseFloat(theStation.diesel_price);
	    //count++;

	    if (isNaN(theMidPrice)){theMidPrice = 0;}
	    if (isNaN(theRegPrice)){theRegPrice = 0;}
	    if (isNaN(thePrePrice)){thePrePrice = 0;}
	    if (isNaN(theDiePrice)){theDiePrice = 0;}

	  //  console.log(theMidPrice,theRegPrice, thePrePrice, theDiePrice);

	    if (theMidPrice < collection.mid.min && theMidPrice != 0) {
		collection.mid.min = theMidPrice;
	    }
	    if (theRegPrice < collection.reg.min && theRegPrice != 0) {
		collection.reg.min = theRegPrice;
	    }
	    if (thePrePrice < collection.pre.min && thePrePrice != 0) {
		collection.pre.min = thePrePrice;
	    }
	    if (theDiePrice < collection.diesel.min && theDiePrice != 0) {
		collection.diesel.min = theDiePrice;
	    }

	    if (theMidPrice > collection.mid.max) {
		collection.mid.max = theMidPrice;
	    }
	    if (theRegPrice > collection.reg.max) {
		collection.reg.max = theRegPrice;
	    }
	    if (thePrePrice > collection.pre.max) {
		collection.pre.max = thePrePrice;
	    }
	    if (theDiePrice > collection.diesel.max) {
		collection.diesel.max = theDiePrice;
	    }

	    if (theMidPrice > 0) {collection.mid.avg += theMidPrice; countMid++; }
	    if (theRegPrice > 0) {collection.reg.avg += theRegPrice; countReg++;}
	    if (thePrePrice > 0) {collection.pre.avg += thePrePrice; countPre++;}
	    if (theDiePrice > 0) {collection.diesel.avg += theDiePrice; countDie++;}

	}
	collection.mid.avg = collection.mid.avg / countMid;
	collection.reg.avg = collection.reg.avg / countReg;
	collection.pre.avg = collection.pre.avg / countPre;
	collection.diesel.avg = collection.diesel.avg / countDie;
	return collection;
    }


    return{
	getAverageGasPrice: getAverageGasPrice,
	currentGasPrices: currentGasPrices
    };

}
