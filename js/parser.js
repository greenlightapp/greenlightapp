
			function readFromPage() {
				var url = "data/";
				url += document.getElementById("myText").value;
				url += ".json";

				var xmlhttp = new XMLHttpRequest();

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						var cardata = jQuery.parseJSON(xmlhttp.responseText);
						parseCarData(cardata);
					}
				};
				xmlhttp.open("GET", url, true);
				xmlhttp.send();
			}

			function parseCarData(arr) {
				var manufacturerArr = new Array();
				var modelArr = new Array();
				for (maker in arr ) {
					manufacturerArr.push(String([maker]));
					for (model in arr[maker]) {
						modelArr.push(String([model]));
						//out += arr[maker][model][0].tran + "<br>";
					}
				}

				$("#maker").autocomplete({
					source : manufacturerArr
				});

				$("#model").autocomplete({
					source : modelArr
				});
			}

			function getFuelEconomy() {
				var url = "data/";
				url += document.getElementById("myText").value;
				url += ".json";

				var xmlhttp = new XMLHttpRequest();

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						var cardata = jQuery.parseJSON(xmlhttp.responseText);
						parseFuel(cardata);
					}
				};
				xmlhttp.open("GET", url, true);
				xmlhttp.send();
			}

			function parseFuel(arr) {
				var car = arr[document.getElementById('maker').value][document.getElementById('model').value][0];
				document.getElementById("id01").innerHTML = "City: " + car.city + "<br>Hwy: " + car.hwy + "<br>Combined: " + car.comb;
			}

